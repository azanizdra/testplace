import {combineReducers} from 'redux'
import {profileReducer} from './profile'
import {profilesReducer} from './profiles'

export const rootReducer = combineReducers({
    profile: profileReducer,
    profiles: profilesReducer
})