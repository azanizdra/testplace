import {GET_PROFILES_REQUEST} from "../actions/ProfilesAction";

const initialState = {
    people: [],
    isFetching: false,
}
export function profilesReducer(state = initialState, action) {

    switch (action.type) {
        case GET_PROFILES_REQUEST:
            console.log('action.payload',state);
            return { ...state, people: action.payload, isFetching: true }

        default:
            return state
    }
}
