import {GET_PROFILES_REQUEST} from "../actions/ProfileAction";

const initialState = {
    name: 'name',
    email: 'test@mail.com',
    image: 'https://randomuser.me/api/portraits/men/57.jpg'

}

export function profileReducer(state = initialState, action) {
    switch (action.type) {
        case GET_PROFILES_REQUEST:
            return { ...state, ...action.payload}

        default:
            return state
    }
}