export const GET_PROFILES_REQUEST = 'GET_PROFILES_REQUEST'

export const getPeople = () => {
    return async(dispatch) => {
        const url: string = 'https://randomuser.me/api/?results=10&noinfo';
        let data =  await fetch(url).then(resp => resp.json()).then(json => json.results);
         dispatch({ type: GET_PROFILES_REQUEST, payload: data });
    }
};

