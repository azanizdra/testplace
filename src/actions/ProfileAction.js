export const GET_PROFILES_REQUEST = 'GET_PROFILE_REQUEST'

export function getProfile(person) {
    return {
        type: GET_PROFILES_REQUEST,
        payload: {
            name: person.name.first,
            email: person.email,
            image: person.picture.large
        },
    }
}