import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import styled from 'styled-components';
import Timer from "./Timer";


const DivImage = styled.div`
    border: none;
    outline: none;    
     height:200px;
    width:200px;
    background-color: ${props => props.currentColor};
   
}
`;

class Color extends React.Component {
    render() {
        const {onClick, value} = this.props;
        return (
            <button onClick={() => onClick()}
                    style={{backgroundColor: value.content}}> {value.title}</button>
        );
    }
}

class Image extends React.Component {
    constructor(props) {
        super(props);

        console.log('props', props);
        this.state = {
            selectedColor: '#ff0',
            colors: props.colors
        }
    }

    handleClick = (id) => {
        this.setState({selectedColor: this.state.colors[id].content});
    }

    render() {
        const {selectedColor, colors} = this.state;
        return (
            <>
                <DivImage currentColor={selectedColor}></DivImage>
                {colors.map((color, id) => <Color key={color.id} onClick={() => this.handleClick(id)}
                                                  value={this.state.colors[id]}/>)}
            </>

        );
    }
}

export default Image;
