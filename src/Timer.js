import React from 'react';
import ReactDOM from 'react-dom';

class Timer extends React.Component {
    constructor(props){
        super(props);
        this.state = {seconds:0};
    }
    tick() {

        this.setState(state => ({
            seconds: state.seconds + 1
        }));
    }
    componentWillMount(){
        this.interval = setInterval(() => this.tick(), 1000);
     //   alert('before_render_once');
    }
    componentDidMount() {
      //  alert('after_render_once');
      //  this.interval = setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render(){
        return <span> Time {this.state.seconds}</span>;
    }
}
export default Timer;

