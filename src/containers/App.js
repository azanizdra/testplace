import React, {Component} from 'react'
import {connect} from 'react-redux'
import './App.css'
import {getPeople} from '../actions/ProfilesAction'
import {getProfile} from '../actions/ProfileAction'
import Profiles from '../components/Profiles';
import {Profile} from '../components/Profile'

export class App extends Component {
    componentDidMount() {
        this.props.getPeopleAction() //
    }

    render() {
        const {getProfileAction, profile, profiles} = this.props
        return (
            <div className="App">
                <header className="App-header">
                    <h1 className="App-title"> My redux application</h1>
                </header>

                <Profiles people={profiles.people} getProfile={getProfileAction}/>

                <Profile name={profile.name} email={profile.email} image={profile.image}/>
            </div>
        )
    }
}

const mapStateToProps = store => {
    return {
        profiles: store.profiles,
        profile: store.profile
    }
}
const mapDispatchToProps = dispatch => ({
    getPeopleAction: () => dispatch(getPeople()),
    getProfileAction: profile => dispatch(getProfile(profile)),

})

export default connect(mapStateToProps, mapDispatchToProps)(App)
