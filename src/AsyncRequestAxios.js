import React from 'react';
import ReactDOM from 'react-dom';
import axios from "axios";

export default class PersonList extends React.Component {
    state = {
        people: []
    }

    componentDidMount() {
        const url: string = 'https://randomuser.me/api/?results=10';
        axios.get(url)
            .then(res => {
                console.log('res', res.data.results);
                const results = res.data.results;
                this.setState({people: results});
            })
    }

    render() {
        return (
            <div>
                <h1>axios list</h1>
                <ul>
                    {this.state.people.map((person, i) => <li key={i}><img
                        src={person.picture.thumbnail}/>{person.name.first} : <b>{person.email}</b></li>)}
                </ul>
            </div>

        )

    }
}