import React from 'react'
import PropTypes from 'prop-types'

export class Profile extends React.Component {
    render() {
        const {name, email, image} = this.props
        return (
            <div>
                <p>Hi, {name} {email} !</p>
                <img src={image}/>
            </div>
        )
    }
}

Profile.propTypes = {
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
}