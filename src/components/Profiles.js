import React from 'react';
import styled from 'styled-components';

const DivProfileList = styled.div`
    border: none;
    outline: none;    
     height:200px;
    width:200px;   
}
`;

class PersonListFetch extends React.Component {
    render() {
        const {people, getProfile} = this.props;

        return (
            <DivProfileList>
                <h1>Fetch</h1>
                <ul>
                    <ul>
                        {people.map((person, i) =>
                            <li key={i} onClick={() => getProfile(person)}><img
                                src={person.picture.thumbnail}/>{person.name.first} : <b>{person.email}</b></li>)}
                    </ul>
                </ul>
            </DivProfileList>
        );
    }
}

export default PersonListFetch;
